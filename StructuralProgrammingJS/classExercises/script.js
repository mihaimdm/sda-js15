// IF-ELSE
// daca(conditie e adevarata) {
//     executa1
// } altfel {
//     (conditia e falsa)
//     executa2
// }

// var nume = "";
// var varsta = 10;

// var verificarea1 = nume !== ""; // false
// var verificarea2 = varsta > 0; // true

// if(verificarea1 && verificarea2) { // false && true => false
//     console.log("Hello, ", nume, " varsta, " , varsta);
// } else if(verificarea1 || verificarea2) { // false || true => true
//     if(verificarea1) { // false
//         console.log("Hello, ", nume);
//     } else {
//         console.log("Hello, World varsta, ", varsta);
//     }
// } else {
//     console.log("Hello, World!");
// }

// var nume = "Mihai";
// var varsta = 0;

// if(nume !== "" && varsta > 0) { // true && false => false
//     console.log("Hello, ", nume, " varsta, " , varsta);
// } else if(nume !== "" || varsta > 0) { // true || false => true
//     if(nume !== "") { // true
//         console.log("Hello, ", nume);
//     } else {
//         console.log("Hello, World varsta, ", varsta);
//     }
// } else {
//     console.log("Hello, World!");
// }

// var nume = "";
// var varsta = 0;

// var verificarea1 = nume !== ""; // false
// var verificarea2 = varsta > 0; // false

// if(verificarea1 && verificarea2) { // false && false => false
//     console.log("Hello, ", nume, " varsta, " , varsta);
// } else if(verificarea1 || verificarea2) { // false || false => false
//     if(verificarea1) {
//         console.log("Hello, ", nume);
//     } else {
//         console.log("Hello, World varsta, ", varsta);
//     }
// } else {
//     console.log("Hello, World!");
// }

// FOR LOOP
// var listaNume = ['Mihai', 'John', 'Erik'];

// for(var iterator = 0; iterator < listaNume.length; iterator++) {
//     console.log("Hello ", listaNume[iterator]);
// }

// for(var iterator = listaNume.length - 1; iterator >= 0; iterator--) {
//     // console.log("iterator ", iterator);
//     console.log("Hello ", listaNume[iterator]);
// }

// for(i in listaNume) {
//     console.log("Hello ", listaNume[i]);
// }

// listaNume.length = 3
// listaNume[0] = Mihai (pozitia 1 in lista)
// i++ <=> i = i + 1 => i=1
// i=0 && 0 < 3 => Hello Mihai
// listaNume[1] = John
// i=1 && 1 < 3 => Hello John
// i++ <=> i = i + 1 => i=2
// listaNume[2] = Erik
// i=2 && 2 < 3 => Hello Erik
// i++ <=> i = i + 1 => i=3
// i=3 && 3 < 3 => false (for loop se opreste)

// WHILE LOOP
// var listaNume = ['Mihai', 'John'];

// var conditie = true;
// while (conditie) {
//     if(listaNume.length === 5) {
//         conditie = false;
//     } else {
//         listaNume.push('AltNume' + (listaNume.length+1));
//     }
// }

// result listaNume => ['Mihai', 'John', 'AltNume3', 'AltNume4', 'AltNume5']

// conditie == true => true
// 3 === 5 => false => else => adauga 'AltNume4' la lista
// conditie == true => true
// 4 === 5 => false => else => adauga 'AltNume5' la lista
// conditie == true => true
// 5 === 5 => true => if => schimba conditie la false
// conditie == true => false => while loop se opreste

// var iterator = 0;
// while(iterator < listaNume.length) {
//     console.log("Hello ", listaNume[iterator]);
//     iterator++;
// }

// listaNume.length = 3
// iterator = 0
// 0 < 3 => Hello Mihai
// iterator++ => iterator = 1
// 1 < 3 => Hello John
// iterator++ => iterator = 2
// 2 < 3 => Hello Erik
// iterator++ -> iterator = 3
// 3 < 3 => false (while loop se opreste)

// Exercitiu print nume complet cu IF ElSE si FOR
// var listaNume = ['Doe'];
// var listaPrenume = ['Mihai', 'Wilson', 'Josh', 'Mike'];

// if(listaNume && listaPrenume) {

//     var lungimeMaxima;
//     if(listaNume.length > listaPrenume.length) {
//         lungimeMaxima = listaNume.length;
//     } else {
//         lungimeMaxima = listaPrenume.length;
//     }

//     var i;
//     for(i = 0; i < lungimeMaxima; i++) {
//         if(listaNume[i] && listaPrenume[i]){
//             console.log("Hello nume si prenume: ", listaNume[i], ' ', listaPrenume[i]);
//         } else {
//             if(listaNume[i]) {
//                 console.log("Hello (doar) nume: ", listaNume[i]);
//             } else {
//                 console.log("Hello (doar) prenume:", listaPrenume[i]);
//             }
//         }
//     }
// } else {
//     console.error("One of the lists is empty");
// }

// var hour = 15;
// if( hour > 6 && hour < 12 ) {
//     console.log("Good morning");
// } else if( hour >= 12 && hour < 18  ){
//     console.log("Good afternoon");
// } else {
//     console.log("Good evening");
// }

// for(var i = 0; i < 10; i++) {
//     console.log("iterator ", i);
// }

// SWITCH
// var color = 'blue';
// switch(color) {
//     case 'blue':
//         console.log("Albastru!");
//         break;
//     case 'red':
//         console.log("Rosu!");
//         break;
//     default:
//         console.log("Alta culoare decat albastru sau rosu!");
//         break;
// }

// if(color == 'blue') {
//     console.log("Albastru!");
// } else if (color == 'red') {
//     console.log("Rosu!");
// } else {
//     console.log("Alta culoare decat albastru sau rosu!");
// }

// if(color == 'blue') {
//     console.log("blue");
// }

// FUNCTIONS
// functii care returneaza ceva => return
// functii care nu returneaza => void
// function suma(a, b) {
//     return a+b;
// }

// console.log(suma(10,5), suma(20, 10));

// function printeazaData() {
//     console.log(new Date()); //new Date() -> printeaza data curenta
// }

// printeazaData();

// ANONYMOUS FUNCTIONS
// var suma = function(a, b) {
//     return a+b;
// }

// console.log(suma(10,5), suma(20, 10));

// CALLBACK

//anonymous function, it does something
// var f = function(a){
//     return a * 5;
// }

// var g = function(a){
//     return a * 10;
// }

// //function, accepts a callback as an argument
// function go(n, func){
//     return func(n);
// }

// //calls the function
// console.log("Result ", go(10, f)); // f(10) => 50
// console.log("Result ", go(20, g)); // g(20) => 200

// //calls the function, this time the whole function is given as an argument
// console.log("Result ", go(30, function(a) {
//     return a*99;
// })) // function(99) => 30 x 99 = 2970

// apelam functia go: n=30 func=function(a) {
//     return a*99;
// }
// functia go returneaza func(n): function(30) {
//     return 30*99;
// }
// functia function(30) returneaza 2970

// VARIABLES SCOPING

// var afara = "Variabila afara (scop global)";
// function scop() {
//     var inauntru = "Variabila inauntru (scop local)";
//     console.log("In functie ", afara);
//     console.log("In functie ", inauntru);
// }

// scop();
// console.log("In afara functiei ", afara);
// console.log("In afara functiei ", inauntru);

// SAME NAME VARIABLE
// var variabila = "Variabila globala";
// function scopLocal() {
//     var variabila = "Variabila locala";
//     console.log("In functie ", variabila);
// }
// scopLocal();
// console.log("In afara functiei ", variabila);

// EXERCITIU SLIDES
// var a = 2; var b = 5; var c = 10;
// console.log(a,b,c); // 2,5,10

// function f(){
//   var a = 99;
//   var b = 999;
//   c = 9999; // suprascriem valoarea gloabala a lui c, deoarece nu definim iar variabila cu var
//   console.log(a,b,c); // 99, 999, 9999
// }

// console.log(a,b,c); // 2,5,10 -> functia f nu a fost apelata, iar variabilele nu au fost schimbate
// f();
// console.log(a,b,c); // 2,5,9999

// CLOSURE
// var count = (function () {
//     var counter = 0;
//     return function (n) {
//         return counter += n;
//     }
// })();

// console.log(count(10));
// console.log(count(15));
// console.log(count(25));
// console.log("counter", counter); // undefined

// var counter = 0;
// function count2(n) {
//     counter = counter + n;
//     return counter;
// }

// console.log(count2(10));
// console.log(count2(15));
// console.log(count2(25));
// console.log("counter2", counter); // 50

// EX1
// function test() {
//   console.log(a); // undefined (pentru ca variabilele trebuie definite inainte sa le folosim)
//   console.log(foo()); // 2 (pentru ca functiile pot fi definite oriunde in scop)

//   var a = 1;
//   function foo() {
//     return 2;
//   }
// }

// test();
// console.log(foo()); // foo is not defined (functiile definite in scop local(test) nu pot fi apelate in scop global)

// EX2
// var a = 1;

// function someFunction(number) { // number = 9
//   function otherFunction(input) {
//     return a;
//   }

//   a = 5; // a = 5 in scop global

//   return otherFunction;
// }

// var firstResult = someFunction(9); // otherFunction
// var result = firstResult(2); // otherFunction(2) => 5
// console.log(result);

// EX3
//varianta 1
// var a = 1;
// function doSomething(n) {
//     a = 10;
//     function calculateSum(n) {
//         return a+n;
//     }

//     return calculateSum(n);
// }

// console.log(doSomething(5)); // rezultatul este 15

//varianta2
// var a = 1;
// function doSomething(n) {
//     function calculateSum(n) {
//         return a+n;
//     }
//     a = 10;

//     return calculateSum(n);
// }

// console.log(doSomething(5)); // rezultatul este tot 15, deoarece valoarea lui a este 10 in momentul in care apelam calculateSum, si nu consideram valoarea lui a din momentul in care am definit functia calculateSum

//varianta3
// var a = 1;
// function doSomething(n) {
//     if(n > 10) {
//         return a;
//     }
//     a = 5;
//     if(n <= 10) {
//         return a*2;
//     }
// }

// console.log("doSomething(5) ", doSomething(5)); // 10 (schimba valoarea globala a lui a in 5)
// console.log("doSomething(15) ", doSomething(15)); // 5 (deoarece functia a fost apelata inainte si valoarea globala a lui a s a schimbat)

// denumirea variabilelelor trebuie sa fie sugestiva/intuitiva
// var culoare="rosu";
// var varsta=10;
// var eAdult = varsta > 18;
// if(eAdult) {
//     console.log("Este adult!");
// }

//PASSING VARIABLES
// obj este un obiect in forma JSON care stocheaza structuri cheie-valoare
// name and price sunt proprietati
// pencil si 10 sunt valori

// definitia obiectului {
//     name: string;
//     price: number;
//     availability: boolean;
// }
// var product1 = { name: "pencil", price: 10, availability: true};
// var product2 = { name: "crayon", price: 20, availability: false};
// var products = [product1, product2];

// in cazul unui for cu 'in', valoarea este doar un iterator(indice) in cazul nostru 0 sau 1 (pozitita produselor din products)
// for(i in products) {
//     console.log(products[i].name, " pret: ", products[i].price, " disponibilitate: ", products[i].availability);
// }

// in cazul unui for cu 'of', valoarea este cea din array, in cazul nostru obiectul cu totul { name: "pencil", price: 10, availability: true}
// for(product of products) {
//     console.log(product.name, " pret: ", product.price, " disponibilitate: ", product.availability);
// }

// products.forEach( product => {
//     console.log(product.name, " pret: ", product.price, " disponibilitate: ", product.availability);
// });