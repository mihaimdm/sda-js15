//constructor - a function, creating an object
function Car(brand, model, price, age){
    //assigning values to an object
    this.brand = brand;
    this.model = model;
    this.price = price;
    this.age = age;
    //methods - functions inside an object
    // countPrice and increasePrice - void (just do action, no return)
    this.countPrice = function() {
        this.price = this.price*100;
    }
    this.increasePrice = function(amount) {
        this.price=this.price+amount;
    }
    // isInRange and copy - return (they return something)
    this.isInRange = function(start, end) {
        if(this.price>=start && this.price <=end){
            return true;
        }else{
            return false;
        }
    }
    //function, copying an object
    this.copy = function(){
        var anotherCar = new Car(this.brand, this.model, this.price, this.age);
        return anotherCar;
    }
}
//sample call
var auto = new Car("ford","ka",50000,10);
console.log('auto ', auto);
auto.countPrice();
console.log(auto.price); //50000 * 100
auto.increasePrice(5000);
console.log(auto.price); //50000 * 100 + 5000 = 5005000
console.log(auto.isInRange(20000,300000000));
//copying
var auto2 = auto.copy();
//test it with changing auto's property, auto2 should remain unchanged
auto.brand = 'changed';
console.log(auto);
console.log(auto2);
auto2 = auto.copy();
console.log(auto);
console.log(auto2);