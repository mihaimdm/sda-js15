// Create a JavaScript object, representing a student.
// Student has: name, surname, age.
// Student has also an array of courses.
// Each course has a name, number of hours and a short description.


// Write a function that will create a sample Course object and fill it with some data.


// Write a function that will create a sample Student object and fill it with some data.


// Write another function that will copy the previously created Student and make a new one.


//Check if the students is properly copied - including his courses (change in copied student's courses should not affect the original ones)!


//changing name of one student, the other one should remain unchanged

//changing name of one student's course, the courses of the other student should remain unchanged


// Create a JavaScript object, representing a student.
// Student has: name, surname, age.
// Student has also an array of courses.
// Each course has a name, number of hours and a short description.
var student1 = {
    name: "Ale",
    surname: "Doe",
    age: "19",
    courses: [
        { name: "german", hours: "5", description: "learn german" },
        { name: "history", hours: "4", description: "learn history" },
    ]
}
// Write a function that will create a sample Course object and fill it with some data.
function Course(name, hours, description) {
    this.name = name;
    this.hours = hours;
    this.description = description;
}
var course1 = new Course("french", "6", "learn french");
console.log(course1);
// Write a function that will create a sample Student object and fill it with some data.
function Student(name, surname, age, courses) {
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.courses = courses;
}
var course2 = new Course ("english", "9", "learn english");
var listcourses = [
    course1, course2
]
// Write another function that will copy the previously created Student and make a new one.
var student2 = new Student ("Ale", "Doe", "30", listcourses);
console.log("student1", student1);
console.log ("student2", student2);
//Check if the students is properly copied - including his courses (change in copied student's courses should not affect the original ones)!
student2.courses = student1.courses;
console.log ("student2", student2);
//changing name of one student, the other one should remain unchanged
student2.name = "Daniela";
console.log(student2.name);
console.log(student1.name);
//changing name of one student's course, the courses of the other student should remain unchanged
student2.courses[0].name = "programare";
console.log(student2.courses);
student2.courses[1].name = "math";
student2.courses[1].description = "learn math";
console.log (student2.courses);