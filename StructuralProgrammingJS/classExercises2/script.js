// PASSING VARIABLES
// EX1
// var obj = { name: "pencil", price: 10 };
// var x = 15;

// function change(product, newPrice) {
//     //changes the object's property
//     product.price = newPrice; // => obj.price = 15

//     //changes the variable's value
//     newPrice = 0; // => x = 0
// }

// change(obj, x); //a function call
// console.log(obj); // { name: "pencil", price: 15 } => the object has changed (passed by reference - arrays and objects)
// console.log(x); // x = 15 => the variable has NOT changed (passed by value - numbers, strings)

// EX2
// var listNames = ['Mihai', 'John'];
// var newName = 'Nick';

// function addName(names, newName) {
//     // names passed by reference
//     // newName passed by value
//     names.push(newName);

//     newName = 'Dereck';
// }

// addName(listNames, newName);
// console.log(listNames); // ['Mihai', 'John', 'Nick'] => the object has changed (passed by reference - arrays and objects)
// console.log(newName); // 'Nick' => the variable has NOT changed even if same name with function argument (passed by value - numbers, strings, boolean)

// LET vs VAR
// for (var j = 0; j < 10; j++) {
//     console.log(j);
// }

// console.log('var j -> ', j); // j = 10

// for (let i = 0; i < 10; i++) {
//     console.log(i);
// }

// console.log('let i -> ', i); //error: let is not available outside the loop

// variabilele setate cu var pot fi disponibile in afara block-scope (dupa if sau for), iar ele nu pot fi disponibile in afara local-scope(dupa function)
// variabilele setate cu const si let nu pot fi disponibile in afara block-scope (dupa if sau for) si in afara local-scope(dupa function)

// let x = 1;

// //some block of code
// if(1 == 1){ // true
//     let x = 2;
//     console.log(x); // 2
// }

// //the same name, a different variable
// console.log(x); // 1

// CONST variables
// Best practice! Constantele se scriu cu litere mari si underscore casing.
// const FLAG_TO_DISABLE_PRICING = true;
// const x = 5;
// x = 10;
// console.log(x); // eroare Uncaught TypeError: Assignment to constant variable. (nu putem modifica o constanta - definita cu const keyword)

// const X = 7;
// if(X == 7){ // true
//     const Y = 8;
//     console.log(X,Y); //7 8
// }
// console.log(X,Y); //error: Y is not defined

// SCOPING: LOCAL vs BLOCK
// -> Global variables last as long as the application is running.
// -> Local variables last as long as a function is running.
// -> Block statements don't create a new scope and the block scope does not apply to var keyword.
// -> Const and Let can define scope to block statements.

// Block scope (in if else sau for loop)
// if(true) {
//     var cat = 'Meow'; // variable set with var can be accessed outside block of code
//     const dog = 'Ham'; // variable set with const and let cannot be accessed outside block of code
//     let sheep = 'Bee';

//     console.log(cat); // 'Meow'
//     console.log(dog); // 'Ham'
//     console.log(sheep); // 'Bee'
// }

// console.log(cat); // 'Meow'
// console.log(dog); // error
// console.log(sheep); // error

// Local scope (in functions)
// function asssignVars() {
//     var cat = 'Meow'; // variable set with var cannot be accessed outside block of code because inside of function there is local scope
//     const dog = 'Ham';
//     let sheep = 'Bee';

//     console.log(cat); // 'Meow'
//     console.log(dog); // 'Ham'
//     console.log(sheep); // 'Bee'
// }

// asssignVars();
// console.log(cat); // error
// console.log(dog); // error
// console.log(sheep); // error

// CONST
// const X = 7;
// // Block scope
// if(X == 7){
//     const X = 8;
//     console.log(X); // 8
// }

// //same name, different scope
// console.log(X); // 7

// // VAR
// var y = 7;
// // Block scope
// if(y == 7){
//     var y = 8;
//     console.log(y); // 8
// }

// //same name, different scope
// console.log(y); // 8

// // LET
// let z = 7;
// // Block scope
// if(z == 7){
//     let z = 8;
//     console.log(z); // 8
// }

// //same name, different scope
// console.log(z); // 7

// HOISTING
//calls a function before its declaration and it works! (doesn't work for anonymous functions)
// work();

// function work(){
//     console.log("it works");
// }

// num = 6;
// console.log(num); // outputs: 6
// var num;

// console.log(num); // outputs: undefined;
// //declaration(only for var) is hoisted, initialization is not
// var num; // declarare - this is hoisted
// num = 6; // initializare - this is not hoisted

// num2 = 6;
// console.log(num2); // outputs: error - num2 does not exist
// var num; // declares num and not num2

// num = 6;
// console.log(num); // outputs: error - does not work for let or const
// let num;

// x(); //error - JS does not hoist ANONYMOUS functions
// var x = function(){
//     console.log("works");
// }

// RECURSION
// printam toate numerele in ordine descrescatoare de la x la 0
// function a(x){
//     console.log(x);

//     //a condition to prevent an endless loop
//     if(x > 0){
//         a(x - 1); //the function calls itself
//     }
// }

// a(4); // 4 3 2 1 0
// a(4) -> printam 4 -> 4 > 0 -> a(4-1)=a(3)
// a(3) -> printam 3 -> 3 > 0 -> a(3-1)=a(2)
// a(2) -> printam 2 -> 2 > 0 -> a(2-1)=a(1)
// a(1) -> printam 1 -> 1 > 0 -> a(1-1)=a(0)
// a(0) -> printam 0 -> 0 > 0 false -> se opreste functia recursiva

// function b(x, n){
//     console.log(x);

//     //a condition to prevent an endless loop
//     if(x > n){
//         b(x - 1, n); //the function calls itself
//     }
// }

// b(4, -3); // 4 3 2 1 0 -1 -2 -3

// Fibonacci
// 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377 ...
// x0 = 0
// x1 = 1
// x2 = x1 + x0 = 1 + 0 = 1
// x3 = x2 + x1 = 1 + 1 = 2
// ...
// x(n)= x(n-1) + x(n-2)

// function fibonacci(n) {
//   if (n == 0) {
//     return 0;
//   }
//   if (n == 1) {
//     return 1;
//   }
//   if (n > 1) {
//     return fibonacci(n - 1) + fibonacci(n - 2);
//   }
// }

// console.log(fibonacci(3));
// fibonacci(3) -> 3 == 0 false -> 3 == 1 false -> 3 > 1 true-> returnam fibonacci(2) + fibonacci(1) -> fibonacci(1) + fibonacci(0) + fibonacci(1) -> 1 + 1 = 2
// fibonacci(2) -> 2 == 0 false -> 2 == 1 false -> 2 > 1 true-> returnam fibonacci(1) + fibonacci(0) -> 1 + 0 = 1
// fibonacci(1) -> 1 == 0 false -> 1 == 1 true -> returnam 1
// fibonacci(0) -> 0 == 0 true -> returnam 0

// Exercitiu print toate fibonacci intre un interval
// function fibonacciInRange(start, end) {
//     for(let i = start; i<= end; i++) {
//         console.log('position: ', i , ' value: ', fibonacci(i));
//     }
// }
// fibonacciInRange(5,30);

// OBJECTS
// var car = {
//     // proprietati (variabile, constante)
//     brand: "ford",
//     model: "fiesta",
//     year: 1999,
//     mileage: 200000,
//     // metode (functii)
//     launch: function() {  // anonymous function
//         console.log("works!");
//     }
// }

// console.log(car.brand); // ford

// car.model = "focus"; //assigns a new value to the property

// console.log(car.model); // focus

// car.launch(); //executes a method (function) from inside the object

// function car(brand, model, year, mileage) {
//     this.brand = brand;
//     this.model = model;
//     this.year = year;
//     this.mileage = mileage;
//     this.launch = function(){
//         console.log("works");
//     }
// }

// var auto1 = new car("fiat","500",2018,10000);
// var auto2 = new car("opel","corsa",2010,100000);
// console.log('auto1: ', auto1);
// console.log('auto2: ', auto2);

// auto1.launch(); //access the object's method
// auto2.year = auto1.year; //access the object's property

// JSON
// Stringify
//simple JS object
// var obj = {
//   id: 1,
//   name: "something",
// }; // sub forma de obiect

// console.log(obj);

// var strObj = JSON.stringify(obj); //converts an object to a JSON string
// console.log(strObj); //we can check if it works
// var againObj = JSON.parse(strObj);
// console.log(againObj); 

// Parse
//a string variable
// var strObj = '{"id":1,"name":"something"}';
// console.log(strObj); 
// console.log(strObj.id); // undefined (doarece nu gaseste proprietatea id pe strObj pt ca strObj este de tipul string/text)

//decoding a string to an object
// var obj = JSON.parse(strObj); 
// //let's see if it works by referring to the object's property
// console.log(obj); 
// console.log(obj.id); // 1

// RECURSION FACTORIAL
// Create a function that will calculate a factorial of a number: n!
// Do it in two ways: by using standard loops and using recursion.

// function calculateNFactorialLoops(n) {
//     var sum = 1;
//     for (var i=1;i<=n;i++){
//           sum = sum*i;
//     }
//     return sum;
// }
// console.log('calculateNFactorialLoops(10) ', calculateNFactorialLoops(10));

// function calculateNFactorialRecursive(n) {
//     if (n == 0){
//         return 1;
//     } else {
//         return n * calculateNFactorialRecursive(n-1);
//     }
// }

// console.log('calculateNFactorialRecursive(10) ', calculateNFactorialRecursive(10));
// console.log('calculateNFactorialRecursive(4) ', calculateNFactorialRecursive(4));
// // calculateNFactorialRecursive(4) => 4 * calculateNFactorialRecursive(3) = 4 * 6 = 24
// // calculateNFactorialRecursive(3) => 3 * calculateNFactorialRecursive(2) = 3 * 2 = 6
// // calculateNFactorialRecursive(2) => 2 * calculateNFactorialRecursive(1) = 2 * 1 = 2
// // calculateNFactorialRecursive(1) => 1 * calculateNFactorialRecursive(0) = 1 * 1 = 1
// // calculateNFactorialRecursive(0) => 1

// RECURSION COUNTDOWN
// Functie care printeaza toate numerele in jos.

function countDown(fromNumber) {
    console.log(fromNumber);
    if(fromNumber != 0) {
        return countDown(fromNumber-1);
    }
}

countDown(4); // 10, 9, 8, ..., 0
// countDown(10) => print 10 -> countDown(9)
// countDown(9) => print 9 -> countDown(8)
// .....
// countDown(1) => print 1 -> countDown(0)
// countDown(0) => print 0 -> se opreste

function countUp(fromNumber) {
    console.log(fromNumber);
    if(fromNumber != 0) {
        return countUp(fromNumber+1);
    } else {
        console.log("Am ajuns la 0!");
    }
}

countUp(-4); // -4, -3, -2, -1, 0
// countup(-4) => print -4 -> countUp(-3)
// countUp(-3) => print -3 -> countUp(-2)
// countUp(-2) => print -2 -> countUp(-1)
// countUp(-1) => print -1 -> countUp(0)
// countUp(0) => print 0 -> se opreste
