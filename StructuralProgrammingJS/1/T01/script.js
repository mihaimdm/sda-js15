
//surfaceArea with callback
function surfaceAreaCallback(shape, a, b, c, d){
    result = shape(a, b, c, d);
    console.log("result ", result);
}

// anonymous functions -> functia este asignata variabilei triangle
var triangle = function(a, b) {
    return a*b/2;
}

// named function
function triangle2(a, b) {
    return a*b/2;
}

var rectangle = function(a,b) {
    return a*b;
}

var trapezoid = function(a, b, c) {
    return (a+b) * c / 2;
}

var altaForma = function(a, b, c, d) {
    return a+b+c+d / 2;
}

//sample function call (apelam)
surfaceAreaCallback(trapezoid, 4, 3, 2); // shape = trapezoid
surfaceAreaCallback(rectangle, 4, 3);

surfaceAreaCallback(triangle, 4, 3);
surfaceAreaCallback(triangle2, 4, 3);

surfaceAreaCallback(altaForma, 4, 3, 2, 1);

console.log("Named function", triangle2(4,3,2));
console.log("Anonymous function", triangle(4,3,2));


